import os
from renpy import exports
from renpy.display import im, image
from modloader import modast, modinfo
from modloader.modclass import Mod, loadable_mod

side_image_crops = {
    "lorem": (300, 0, 500, 500, 250, 250),
    "remy": (5, 30, 500, 600, 250, 300),
    "anna": (30, 35, 500, 600, 250, 300),
    "adine": (50, 0, 500, 600, 250, 300),
    "bryce": (400, 40, 500, 600, 250, 300),
    "maverick": (0, 0, 500, 600, 250, 300),
    "sebastian": (70, 0, 500, 600, 250, 300),
    "reza": (375, 0, 500, 600, 250, 300),
    "zhong": (50, 0, 500, 600, 250, 300),
    "vara": (0, 0, 500, 500, 250, 250),
    "amely": (150, 0, 500, 500, 250, 250),
    "damion": (30, 0, 500, 600, 250, 300),
    "emera": (0, 0, 500, 600, 250, 300),
    "ipsum": (100, 40, 500, 600, 250, 300),
    "kevin": (40, 0, 500, 600, 250, 300),
    "katsu": (0, 200, 500, 600, 250, 300),
    "izumi": (35, 0, 250, 300, 250, 300),
    "xith": (15, 25, 500, 600, 250, 300),
    "ophinia": (0, 0, 500, 600, 250, 300),
    "miles": (50, 0, 500, 600, 250, 300),
    "leymas": (85, 20, 500, 600, 250, 300),
    "8th": (80, 15, 500, 600, 250, 300),
    "dramavian": (90, 165, 500, 600, 250, 300),
    "kalinth": (80, 100, 500, 600, 250, 300),
    "lucius": (15, 15, 500, 600, 250, 300),
    "grey": (50, 165, 500, 600, 250, 300),
    "heinz": (40, 0, 250, 300, 250, 300),
    "francois": (10, 0, 250, 300, 250, 300),
    "sybil": (70, 90, 250, 300, 250, 300),
    "naomi": (70, 80, 500, 600, 250, 300)
}

@loadable_mod
class AWSWMod(Mod):
    name = "CRAP"
    version = "0.1.1"
    author = "AwSW Community"
    dependencies = ["?Side Images"]
    
    def mod_load(self):
        side_images = modinfo.has_mod("Side Images")
        path = modinfo.get_mod_path("CRAP")
        loaded_cr_imgs = {}
        loaded_cr_flip_imgs = {}
        
        def for_each_image(folder, func):
            folder_path = os.path.join(path, "resource", folder)
            if not os.path.isdir(folder_path):
                return
            for root, _, files in os.walk(folder_path):
                for filename in files:
                    if not os.path.isdir(os.path.join(folder_path, root, filename)):
                        func(filename, os.path.relpath(os.path.join(folder, root, filename), start=os.path.join(path, "resource")).replace("\\", "/"))
        
        def define_static_images(fn, pth):
            exports.image(fn[:fn.rfind(".")], im.Image(pth))

        def define_cr_overlay_images(fn, pth):
            before_fn, after_fn = fn[:fn.rfind(".")].split("+")
            before_image_names = before_fn.split("_")

            before_image_pth = os.path.join(os.path.dirname(pth), before_fn + ".png").replace("\\", "/")
            composite = im.Composite(None, (0, 0), before_image_pth, (0, 0), pth)

            if before_image_names[-1] == 'flip':
                after_image_names = tuple(before_image_names[:-1] + list(after_fn.split("_")) + ['flip'])
                loaded_cr_flip_imgs[after_image_names[:-1]] = composite
            else:
                after_image_names = tuple(before_image_names + list(after_fn.split("_")))
                loaded_cr_imgs[after_image_names] = composite

            exports.image(" ".join(after_image_names), composite)

        def define_cr_images(fn, pth):
            if '+' in fn:
                define_cr_overlay_images(fn, pth)
            else:
                image_names = tuple(fn[:fn.rfind(".")].split("_"))
                exports.image(" ".join(image_names), im.Image(pth))
                
                if image_names[-1] == "flip":
                    loaded_cr_flip_imgs[image_names[:-1]] = pth
                else:
                    loaded_cr_imgs[image_names] = pth

        def create_side_image(image_names, pth, flip_img):
            if side_images and image_names[0] in side_image_crops:
                crops = side_image_crops[image_names[0]]
                side_img = im.Scale(im.Crop(pth, crops[:4]), *crops[4:]) 
                exports.image("side " + " ".join(image_names), im.Flip(side_img, horizontal=True) if flip_img else side_img)
        
        for_each_image("bg", define_static_images)
        for_each_image("cg", define_static_images)
        for_each_image("cr", define_cr_images)
        
        for image_names, pth in loaded_cr_imgs.items():
            if image_names in loaded_cr_flip_imgs:
                create_side_image(image_names, loaded_cr_flip_imgs[image_names], False)
            else:
                exports.image(" ".join(image_names) + " flip", im.Flip(pth, horizontal=True))
                create_side_image(image_names, pth, True)
    
    def mod_complete(self):
        pass
